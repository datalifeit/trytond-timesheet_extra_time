# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import timesheet
from . import company
from . import team_timesheet
from . import profile


def register():
    Pool.register(
        timesheet.Timesheet,
        company.Employee,
        company.EmployeeCostPrice,
        module='timesheet_extra_time', type_='model')
    Pool.register(
        profile.ProfilePayrollCost,
        timesheet.TimesheetProfile,
        module='timesheet_extra_time', type_='model',
        depends=['staff_profile'])
    Pool.register(
        team_timesheet.TeamTimesheetEmployee,
        team_timesheet.EditWorkTimesheetStart,
        team_timesheet.TimesheetTeamWorkCompanyEmployee,
        team_timesheet.TeamTimesheet,
        module='timesheet_extra_time', type_='model',
        depends=['team_timesheet'])
