# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from datetime import timedelta
from trytond.tools import reduce_ids
from trytond import backend
from sql.aggregate import Sum


class TeamTimesheetEmployee(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-company.employee'

    extra_duration = fields.Function(fields.TimeDelta('Extra duration'),
        'get_extra_duration')

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetEmployee, cls).__setup__()
        cls.extra_duration._field.converter = cls.duration._field.converter

    @classmethod
    def get_extra_duration(cls, records, name):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')
        twe = TimesheetWorkEmployee.__table__()
        cursor = Transaction().connection.cursor()

        res = {r.id: None for r in records}
        records_ids = []
        for record in records:
            res[record.id] = timedelta(0)
            if record.team_timesheet.state == 'done':
                extra_duration = sum([ts.extra_duration.total_seconds()
                    for ts in record.timesheets if ts.extra_duration])
                res[record.id] = timedelta(seconds=extra_duration)
            else:
                records_ids.append(record.id)

        if records_ids:
            cursor.execute(*twe.select(
                twe.tts_employee,
                Sum(twe.extra_duration),
                where=reduce_ids(twe.tts_employee, records_ids),
                group_by=(twe.tts_employee)))
            for record_id, duration in cursor.fetchall():
                if backend.name == 'sqlite':
                    duration = timedelta(seconds=duration)
                res[record_id] = duration

        return res

    @classmethod
    def get_duration(cls, records, name):
        res = super().get_duration(records, name)
        for record in records:
            if record.team_timesheet.state != 'done' and record.extra_duration:
                res[record.id] += record.extra_duration

        return res


class EditWorkTimesheetStart(metaclass=PoolMeta):
    __name__ = \
        'timesheet.team.timesheet-timesheet.work.edit_timesheets.start'

    @fields.depends('timesheets')
    def on_change_with_total_duration(self):
        seconds = super().on_change_with_total_duration().total_seconds()
        if self.timesheets:
            extra_seconds = sum([twe.extra_duration.total_seconds()
                for twe in self.timesheets if twe.extra_duration])
            seconds = seconds + extra_seconds

        return timedelta(seconds=seconds)


class TeamTimesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet'

    @classmethod
    def _create_tts_work_employee(cls, tts_work, tts_employee):
        twe = super()._create_tts_work_employee(tts_work, tts_employee)
        twe.extra_duration = timedelta(0)

        return twe


class TimesheetTeamWorkCompanyEmployee(metaclass=PoolMeta):
    __name__ = 'timesheet.team.work-company.employee'

    extra_duration = fields.TimeDelta("Extra duration",
        converter='absolute_hours_time')

    def on_change_total_hours(self):
        self.extra_duration = None
        super().on_change_total_hours()

    @classmethod
    def _create_timesheets(cls, record):
        line = super()._create_timesheets(record)
        line.extra_duration = record.extra_duration

        return line

    @fields.depends(methods=['get_total_hours'])
    def on_change_extra_duration(self):
        self.total_hours = self.get_total_hours()

    @fields.depends('extra_duration')
    def get_total_hours(self, name=None):
        if self.extra_duration:
            return super().get_total_hours(name) + (
                self.extra_duration.total_seconds() / 3600)
        return super().get_total_hours(name)
