==============================================
Timesheet Extra Time Scenario Payroll Profile
==============================================

Imports::

    >>> import datetime
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)


Install timesheet_extra_time::

    >>> config = activate_modules(['timesheet_extra_time', 'staff_profile'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get Models::

    >>> Employee = Model.get('company.employee')
    >>> Party = Model.get('party.party')
    >>> Work = Model.get('timesheet.work')
    >>> Line = Model.get('timesheet.line')
    >>> Profile = Model.get('staff.profile')
    >>> ProfilePrice = Model.get('staff.profile.payroll_price')


Create employee::

    >>> party = Party(name='Employee 1')
    >>> party.save()

    >>> employee = Employee(company=company, party=party)
    >>> employee.save()

    >>> price = employee.cost_prices.new()
    >>> price.date = yesterday
    >>> price.cost_price = Decimal(8.50)
    >>> price.extra_cost_price = Decimal(10.50)
    >>> employee.save()


Create work::

    >>> work = Work(name='Work 1')
    >>> work.save()


Create profile::

    >>> profile = Profile(name='Profile 1')
    >>> cost = profile.payroll_prices.new()
    >>> cost.date = today
    >>> cost.payroll_price = Decimal('10.00')
    >>> cost.cost_price=Decimal('9.50')
    >>> cost.extra_cost_price=Decimal('15.50')
    >>> profile.save()

    >>> employee.profile = profile
    >>> employee.save()


Create timesheet lines::

    >>> line = Line(employee=employee, date=today, work=work,
    ...     duration=datetime.timedelta(hours=8))
    >>> line.save()

    >>> line2 = Line(employee=employee, date=today, work=work,
    ...     duration=datetime.timedelta(hours=6))
    >>> line2.save()

    >>> line3 = Line(employee=employee, date=yesterday, work=work,
    ...     duration=datetime.timedelta(hours=4), type='extra')
    >>> line3.save()

    >>> line4 = Line(employee=employee, date=today, work=work,
    ...     duration=datetime.timedelta(hours=2), type='extra')
    >>> line4.save()

    >>> normal_lines = Line.find([('type', '=', 'normal')])
    >>> extra_lines = Line.find([('type', '=', 'extra')], order=[('date', 'DESC')])
    >>> len(normal_lines)
    2

    >>> normal_lines[0].cost_price
    Decimal('9.50')
    >>> len(extra_lines)
    2
    >>> extra_lines[0].cost_price
    Decimal('15.50')