=============================
Timesheet Extra Time Scenario
=============================

Imports::

    >>> import datetime
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)


Install timesheet_extra_time::

    >>> config = activate_modules('timesheet_extra_time')


Get Models::

    >>> Employee = Model.get('company.employee')
    >>> Line = Model.get('timesheet.line')
    >>> Party = Model.get('party.party')
    >>> Work = Model.get('timesheet.work')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create party::

    >>> party = Party(name='Employee 1')
    >>> party.save()


Create employee::

    >>> employee = Employee(company=company, party=party)
    >>> employee.save()

    >>> price = employee.cost_prices.new()
    >>> price.date = yesterday
    >>> price.cost_price = Decimal(8)
    >>> price.extra_cost_price = Decimal(10)
    >>> price = employee.cost_prices.new()
    >>> price.date = today
    >>> price.cost_price = Decimal(8.50)
    >>> price.extra_cost_price = Decimal(10.50)
    >>> employee.save()


Create work::

    >>> work = Work(name='Work 1')
    >>> work.save()


Create timesheet lines::

    >>> line = Line(employee=employee, date=yesterday, work=work,
    ...     duration=datetime.timedelta(hours=8))
    >>> line.save()

    >>> line2 = Line(employee=employee, date=today, work=work,
    ...     duration=datetime.timedelta(hours=6))
    >>> line2.save()

    >>> line3 = Line(employee=employee, date=yesterday, work=work,
    ...     duration=datetime.timedelta(hours=4), type='extra')
    >>> line3.save()

    >>> line4 = Line(employee=employee, date=today, work=work,
    ...     duration=datetime.timedelta(hours=2), type='extra')
    >>> line4.save()


Check timesheet lines::

    >>> normal_lines = Line.find([('type', '=', 'normal')], order=[('date', 'ASC')])
    >>> len(normal_lines)
    2
    >>> [l.cost_price for l in normal_lines]
    [Decimal('8'), Decimal('8.5')]

    >>> extra_lines = Line.find([('type', '=', 'extra')], order=[('date', 'ASC')])
    >>> len(extra_lines)
    2
    >>> [l.cost_price for l in extra_lines]
    [Decimal('10'), Decimal('10.5')]
