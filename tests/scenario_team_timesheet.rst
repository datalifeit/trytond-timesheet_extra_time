=======================
Team Timesheet Scenario
=======================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from datetime import timedelta
    >>> from trytond.modules.company_employee_team.tests.tools import create_team, get_team


Install team_timesheet Module::

    >>> config = activate_modules(['timesheet_extra_time', 'team_timesheet'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get models::

    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> Work = Model.get('timesheet.work')
    >>> TTSWorkEmployee = Model.get('timesheet.team.work-company.employee')


Get today date::

    >>> import datetime
    >>> today = datetime.date.today()


Create team::

    >>> _ = create_team()
    >>> team = get_team()


Create work::

    >>> work = Work(name='Work 1')
    >>> work.save()


Create team timesheet::

    >>> team_timesheet = TeamTimesheet(date=today, team=team)
    >>> team_timesheet.save()

    >>> tts_work = team_timesheet.works.new()
    >>> tts_work.work = work
    >>> tts_work.time_type = 'employee'
    >>> tts_work.duration = timedelta(hours=8)
    >>> team_timesheet.save()


Add extra duration in tts_work::

    >>> team_timesheet = TeamTimesheet(team_timesheet.id)
    >>> tts_work, = team_timesheet.works

    >>> edit_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.edit_timesheets', [tts_work])
    >>> edit_work_timesheets.form.duration.total_seconds() / 3600
    8.0
    >>> timesheet, = edit_work_timesheets.form.timesheets
    >>> timesheet.extra_duration = timedelta(hours=2.5)
    >>> timesheet.total_hours
    10.5
    >>> timesheet.total_hours = 10.0
    >>> bool(timesheet.extra_duration)
    False
    >>> timesheet.duration == timedelta(hours=10)
    True
    >>> timesheet.duration = timedelta(hours=8)
    >>> timesheet.extra_duration = timedelta(hours=1)
    >>> timesheet.total_hours
    9.0
    >>> edit_work_timesheets.execute('edit_timesheets')


Edit extra duration in tts_employee::

    >>> team_timesheet = TeamTimesheet(team_timesheet.id)
    >>> tts_employee, = team_timesheet.employees

    >>> edit_employee_timesheets = Wizard('timesheet.team.timesheet-company.employee.edit_timesheets', [tts_employee])
    >>> timesheet, = edit_employee_timesheets.form.timesheets
    >>> timesheet.extra_duration.total_seconds() / 3600
    1.0
    >>> timesheet.extra_duration = timedelta(hours=2.5)
    >>> timesheet.total_hours
    10.5
    >>> edit_employee_timesheets.execute('edit_timesheets')


Check times in tts_employee::

    >>> team_timesheet = TeamTimesheet(team_timesheet.id)
    >>> tts_employee, = team_timesheet.employees
    >>> tts_employee.extra_duration.total_seconds() / 3600
    2.5
    >>> tts_employee.duration.total_seconds() / 3600
    10.5
    >>> tts_employee.day_duration.total_seconds() / 3600
    10.5


End team timesheet::

    >>> team_timesheet.click('wait')
    >>> team_timesheet.click('confirm')
    >>> team_timesheet.click('do')
    >>> team_timesheet.state
    'done'


Check timesheet lines in team timesheet::

    >>> normal, = [t for t in team_timesheet.timesheets if t.type == 'normal']
    >>> normal.duration.total_seconds() / 3600
    8.0

    >>> extra, = [t for t in team_timesheet.timesheets if t.type == 'extra']
    >>> extra.duration.total_seconds() / 3600
    2.5
    >>> (extra.tts_work, extra.employee, extra.date, extra.work) == (
    ...         normal.tts_work, normal.employee, normal.date, normal.work)
    True


Check times in tts_employee::

    >>> tts_employee, = team_timesheet.employees
    >>> tts_employee.extra_duration.total_seconds() / 3600
    2.5
    >>> tts_employee.duration.total_seconds() / 3600
    10.5
    >>> tts_employee.day_duration.total_seconds() / 3600
    10.5
